import rclpy

from nav_msgs.msg import Odometry
from std_msgs.msg import String
from rclpy.node import Node
from example_interfaces.srv import AddTwoInts


class MinimalPublisher(Node):
    def __init__(self):
        super().__init__('example_node')
        self.declare_parameter("param_1", 0.1)
        self.declare_parameter("param_2", 20)
        self.declare_parameter("param_3", "test")

        self.param_1 = self.get_parameter('param_1').get_parameter_value().double_value
        self.param_2 = self.get_parameter('param_2').get_parameter_value().integer_value
        self.param_3 = self.get_parameter('param_3').get_parameter_value().string_value

        self.get_logger().info('Param_1: "%s"' % self.param_1)
        self.get_logger().warn('Param_2: "%s"' % self.param_2)
        self.get_logger().error('Param_3: "%s"' % self.param_3)

        self.str_subscriber = self.create_subscription(String, 'str', self.str_callback, 1)
        self.odom_pub = self.create_publisher(Odometry, 'odom', 1)

        self.srv = self.create_service(AddTwoInts, 'test_srv', self.add_two_ints_callback)

        self.odom_timer = self.create_timer(0.1, self.odom_timer_callback)

    def add_two_ints_callback(self, req, res):
        self.get_logger().warn('Service got: "%s"' % req)
        res.sum = req.a + req.b
        return res

    def odom_timer_callback(self):
        msg = Odometry()
        msg.header.frame_id = "odom"
        msg.child_frame_id = "base_link"
        msg.pose.pose.position.x = 10.0
        msg.pose.pose.orientation.x = 20.0
        self.odom_pub.publish(msg)
        
    def str_callback(self, msg):
        self.get_logger().info('Sub gets: "%s"' % msg.data)


def main(args=None):
    rclpy.init(args=args)
    minimal_publisher = MinimalPublisher()
    rclpy.spin(minimal_publisher)

    minimal_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()

