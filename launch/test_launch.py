import os

from launch import LaunchDescription
from launch_ros.actions import Node
from launch.substitutions import LaunchConfiguration
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
    param_dir = LaunchConfiguration(
        'param_dir',
        default=os.path.join(
        get_package_share_directory('example_ros2'),
        'params',
        'param_1.yaml'))

    return LaunchDescription([
        Node(
            package='example_ros2',
            executable='node_1',
            name='node_1',
            parameters=[param_dir],
            output='screen',
        ),
    ])